from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    name = models.CharField(max_length=50,verbose_name='Category name')
    title = models.CharField(max_length=50,verbose_name='Category')
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'categories'
    def __unicode__(self):
        return self.title

class Tag(models.Model):
    name = models.CharField(max_length=50,verbose_name='Tag name')
    title = models.CharField(max_length=50,verbose_name='Tag')
    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'tags'
    def __unicode__(self):
        return self.title

class Tasks(models.Model):
    user = models.ForeignKey(User, related_name='tasks',verbose_name='Author')
    title = models.CharField(max_length=100,verbose_name='Task name')
    description = models.TextField()
    create_date = models.DateTimeField(auto_now_add=True,verbose_name='Create date')
    end_date = models.DateTimeField(blank=True, verbose_name='Sroki')
    category = models.ForeignKey(Category,related_name='tasks',verbose_name='category')
    tags = models.ManyToManyField(Tag,related_name='tasks',verbose_name=u'Tags')
    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'
    def __unicode__(self):
        return self.title
